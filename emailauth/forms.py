from django.forms import CharField, ModelForm, PasswordInput, ValidationError
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import gettext as _

User = get_user_model()

class UserCreationForm(ModelForm):
    error_messages = {
        'password_mismatch': _('The two password fields didn\'t match.'),
    }
    password1 = CharField(
        label=_('Password'),
        widget=PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
        strip=False)
    password2 = CharField(
        label=_('Password confirmation'),
        widget=PasswordInput,
        help_text=_('Enter the same password as before, for verification.'),
        strip=False)

    class Meta:
        model = User
        fields = ('email',)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise ValidationError(_('A user with that email already exists.'))

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise ValidationError(_('Passwords don''t match'))
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(ModelForm):
    password = ReadOnlyPasswordHashField(
        label=_('Password'),
        help_text=_(
            "Raw passwords are not stored, so there is no way to see this "
            "user's password, but you can change the password using "
            "<a href=\"../password/\">this form</a>."),)

    rel_password_url = None

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        permission_field = self.fields.get('user_permissions', None)
        if permission_field is not None:
            permission_field.queryset = (
                permission_field.queryset.select_related('content_type'))

    def clean_password(self):
        return self.initial['password']
