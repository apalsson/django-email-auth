from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db.models import BooleanField, CharField, DateTimeField, EmailField, Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

class EmailUserManager(BaseUserManager):

    def create_user(self, email, password=None, is_staff=False, **extra_fields):
        if not email:
            raise ValueError(_('Email address is required'))
        email = EmailUserManager.normalize_email(email)
        extra_fields.pop('username', None)  # username field is unnecessary
        user = self.model(email=email, is_staff=is_staff, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        return self.create_user(email, password, is_staff=True, is_superuser=True, **extra_fields)

    def users(self):
        return self.get_queryset().filter(Q(is_staff=False) & Q(is_superuser=False))

    def staff(self):
        return self.get_queryset().filter(is_staff=True)

class AbstractEmailUser(AbstractBaseUser, PermissionsMixin):
    email = EmailField(_('email address'), unique=True)
    first_name = CharField(max_length=30, blank=True)
    last_name = CharField(max_length=150, blank=True)
    is_staff = BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Determines if the user can log into the admin site.'))
    is_active = BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Determines if this user should be treated as active. '))
    date_joined = DateTimeField(_('date joined'), default=timezone.now)

    objects = EmailUserManager()

    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'email'

    class Meta:
        abstract = True
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def clean(self):
        email = self.email
        super(AbstractEmailUser, self).clean()
        self.email = self.__class__.objects.normalize_email(email)

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Sends an email to this User."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

class EmailUser(AbstractEmailUser):
    class Meta(AbstractEmailUser.Meta):
        swappable = 'AUTH_USER_MODEL'
