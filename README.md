## Django Email Authentication

This module changes the Django authentication to use an Email address
for authentication instead of the standard user name.

The implementation is close to the original Django implementation,
with the only difference being the email-address used as username. 

This work is licensed under the 3-clause BSD license,
for more information see LICENSE.txt

### Prerequisites

Django v3.2 and Python v3.6 (or higher)

Note: This module was not tested with Django v4.0.

### Building and installing from source

To install the module in the current virtual environment.

    python3 setup.py clean --all install

To instead install in your home directory.

    python3 setup.py clean --all install --user

### Configuration settings

In your Django settings, add 'emailauth' to your `INSTALLED_APPS`

Then set `AUTH_USER_MODEL` to 'emailauth.EmailUser'

### Installation in local package index (e.g. DevPi) 

The commands below builds and install the artifacts in a local package index
(DevPi) at http://devpi.myserver.lan/ under the `staging` index. 

    python3 setup.py clean --all sdist bdist_wheel
    python3 -m pip install --upgrade twine
    python3 -m twine register --verbose --repository staging dist/*tar.gz
    python3 -m twine upload --repository-url http://devpi.myserver.lan/ --repository staging dist/*
